<?php

for ($i = 1; $i <= 100; $i++) {
    if ($i % 12 == 0) {
        echo 'Hanunga, ';
    } elseif ($i % 4 == 0) {
        echo 'Retut, ';
    } elseif ($i % 3 == 0) {
        echo 'Nureek, ';
    } else {
        echo $i . ', ';
    }
}
