- **Nureek!, Retut!, Hanunga!**

Write a PHP script that prints out the numbers 1 to 100 inclusive, except when:
On every number that is divisible exactly by 3 print the word ‘Nureek’.
On every number that is divisible exactly by 4 print the word ‘Retut’.
On every number that is divisible exactly by 4 and 3 print the word 'Hanunga'.

Example output: 1, 2, Nureek, Retut, 5, Nureek, 7, Retut, Nureek, 10, 11, Hanunga 

**Answer is fizzbuzz.php**

Output:

1, 2, Nureek, Retut, 5, Nureek, 7, Retut, Nureek, 10, 11, Hanunga, 13, 14, Nureek, Retut, 17, Nureek, 19, Retut, Nureek, 22, 23, Hanunga, 25, 26, Nureek, Retut, 29, Nureek, 31, Retut, Nureek, 34, 35, Hanunga, 37, 38, Nureek, Retut, 41, Nureek, 43, Retut, Nureek, 46, 47, Hanunga, 49, 50, Nureek, Retut, 53, Nureek, 55, Retut, Nureek, 58, 59, Hanunga, 61, 62, Nureek, Retut, 65, Nureek, 67, Retut, Nureek, 70, 71, Hanunga, 73, 74, Nureek, Retut, 77, Nureek, 79, Retut, Nureek, 82, 83, Hanunga, 85, 86, Nureek, Retut, 89, Nureek, 91, Retut, Nureek, 94, 95, Hanunga, 97, 98, Nureek, Retut, 

- **A man a plan Panama.**

**Palindromes** are words or sentences that read the same forward as backwards. All punctuation is removed when 
creating a palindrome. Create a class called palindrome that given a string input will return boolean true if it is a
  palindrome or boolean false if it is not.
  Call the class with these inputs and check the results match.
  1. racecar // true
  2. a man a plan a canal Panama. // true
  3. R2D2 // false
  4. Desserts, I stressed! // true
  5. Red rum, sir, is murder. // true
  6. This is the final one // false
  
  **Answer is palindrome.php**
 
- **Triple seventeens fly high!**

Write a PHP script that prints out the numbers in range from 0 to 1000 inclusive, that can be
divided by 3 and has sum of digits of not more than 17.

E.g. 303 is divided by 3 and has sum of digits 3 + 0 + 3 = 6.

**Answer is numbers.php**