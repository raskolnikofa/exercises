<?php

class Palindrome
{
    // check if string is palindrome
    // return string
    public function check($string) {
        $clean_string = strtolower(preg_replace('/[^a-zA-Z]+/', '', $string));
        $revert_string = strrev($clean_string);

        echo $clean_string == $revert_string ? 'true' : 'false';
    }
}

$palindrome = new Palindrome;
$palindrome->check('Red rum, sir, is murder.');